package model;

import java.util.Random;

public class TradeFactory {
	
	private static final String[] entities={"PARIS","LONDON","MIDLOH","NY"};
	private static final  String[] ccys ={"GBR","USD","JPY","EUR","HKD"};
	private static final  String[] tradeIds={"SWAP","EXOTIC"};
	private static final  String[] parties={"HSBC","BOOK"};
	
	private static final  Random random=new Random();
	
	private static  int seed = random.nextInt();
	
	public static Trade get(int seq){
		
		System.out.println("seq "+seq+" random "+seed);
		String legalEntity= entities[ Math.abs(seq) % entities.length];
		String currency= ccys[ Math.abs(seq+seed) % ccys.length];
		seed=random.nextInt();
		String tradeId= tradeIds[Math.abs(seed) % tradeIds.length];
		seed=random.nextInt(seq);
		String counterParty=parties[Math.abs(seed)%parties.length];
		int amount=seed;
		
		int tradeVersion= (seed+seq)%8;
		
		return new Trade(tradeId,tradeVersion,legalEntity,counterParty,currency,amount);
	}

}
