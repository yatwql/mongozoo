package model;

public class Trade {
	
	private String tradeId;
	private int tradeVersion;
	private String legalEntity;
	private String counterParty;
	private String currency;
	private double amount;
	
	
	private String template="<data tradeId='%s' tradeVersion='%d' legalEntity='%s' counterParty='%s' ><ccy>%s</ccy><amount>%f</amount></data>";
	
	
	protected Trade (String tradeId,int tradeVersion,String legalEntity,String counterParty,String currency,double amount){
		this.tradeId=tradeId;
		this.tradeVersion=tradeVersion;
		this.legalEntity=legalEntity;
		this.counterParty=counterParty;
		this.currency=currency;
		this.amount=amount;
	}
	
	public String getTradeId() {
		return tradeId;
	}
	public void setTradeId(String tradeId) {
		this.tradeId = tradeId;
	}
	public int getTradeVersion() {
		return tradeVersion;
	}
	public void setTradeVersion(int tradeVersion) {
		this.tradeVersion = tradeVersion;
	}
	public String getLegalEntity() {
		return legalEntity;
	}
	public void setLegalEntity(String legalEntity) {
		this.legalEntity = legalEntity;
	}
	public String getCounterParty() {
		return counterParty;
	}
	public void setCounterParty(String counterParty) {
		this.counterParty = counterParty;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	
	
	public String getXml(){
	  return String.format(template, tradeId,tradeVersion,legalEntity,counterParty,currency,amount);	
	}
	

}
