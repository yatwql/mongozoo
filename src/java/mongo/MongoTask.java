package mongo;

import java.net.UnknownHostException;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.Mongo;

public abstract class MongoTask {
	protected DBCollection coll;
	
	abstract void process(DBCollection coll);
	
	abstract String getTaskName();
	
	public void exec(){
		Mongo m;
		try {
			m = new Mongo( "localhost" );
			DB db = m.getDB( "trades" );
			DBCollection coll = db.getCollection("trades");
			long before,after;
			before=System.currentTimeMillis();
			System.out.println(String.format("Begin task - %s! now is %d  ",getTaskName(),before));
			process(coll);
			after=System.currentTimeMillis();
			System.out.println(String.format("Completed task - %s! now is %d ,total duration %d ms ",getTaskName(),after,(after-before)));
			
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

}
