package mongo;

import model.Trade;
import model.TradeFactory;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;

public class DataGenerator extends MongoTask{
	
	static final int TOTAL=10000;

	
	@Override
	void process(DBCollection coll) {
		coll.drop();
		System.out.println("Total number before insert data : "+coll.getCount());
		for (int seq=1;seq<=TOTAL;seq++){
			Trade trade=TradeFactory.get(seq);
			BasicDBObject doc = new BasicDBObject();

	        doc.put("tradeId", trade.getTradeId());
	        doc.put("tradeVersion", trade.getTradeVersion());
	        doc.put("legalEntity", trade.getLegalEntity());
	        doc.put("counterParty", trade.getCounterParty());
	        doc.put("currency", trade.getCurrency());

	        doc.put("amount", trade.getAmount());
	        doc.put("xml", trade.getXml());

	        coll.insert(doc);
		}
		System.out.println("Total number after insert data : "+coll.getCount());
		
	}

	@Override
	String getTaskName() {
		// TODO Auto-generated method stub
		return "data generator";
	}

}
