package mongo;

import java.util.*;

import com.mongodb.DBCollection;



public class Bucket extends MongoTask {
	
	private List<MongoTask> tasks=new ArrayList<MongoTask>();

	public void clear(){
		tasks.clear();
	}
	public void addTask(MongoTask task){
		tasks.add(task);
	}
	@Override
	void process(DBCollection coll) {
		for (MongoTask task:tasks){
			task.exec();
		}
		
	}

	@Override
	String getTaskName() {
		StringBuilder builder=new StringBuilder(" Bucket : ");
		for (MongoTask task:tasks){
			builder.append(" - "+task.getTaskName()+" ");
		}
		return builder.toString();
	}

}
