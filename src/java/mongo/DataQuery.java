package mongo;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;

public class DataQuery extends MongoTask {

	@Override
	void process(DBCollection coll) {
		BasicDBObject query1 = new BasicDBObject();

		query1.put("legalEntity", "PARIS");
		
		BasicDBObject query2 = new BasicDBObject();

		query2.put("legalEntity", "MIDLOH");
		

		BasicDBObject query=  new BasicDBObject(); 
		
		query.put("legalEntity", "PARIS");
		
		//query.put("$or", query2);
		DBCursor cursor = coll.find(query);
		int number = 0;
		try {
			while (cursor.hasNext()) {
				number++;
				System.out.println(cursor.next());
			}
		} finally {
			cursor.close();
		}

		System.out.println("Total number of query records " + number);

	}

	@Override
	String getTaskName() {

		return "query";
	}

}
