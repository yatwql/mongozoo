package mongo;

import com.mongodb.DBCollection;

public class DataCleaner extends MongoTask {

	@Override
	void process(DBCollection coll) {
		coll.drop();

	}

	@Override
	String getTaskName() {
		// TODO Auto-generated method stub
		return "cleaner";
	}

}
