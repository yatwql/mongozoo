import mongo.*;



public class MongoMain {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Bucket bucket =new Bucket();
		
		bucket.addTask(new DataCleaner());
		bucket.addTask(new DataGenerator());
		bucket.addTask(new DataQuery());
		bucket.exec();
	}

}
